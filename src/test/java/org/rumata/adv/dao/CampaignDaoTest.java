package org.rumata.adv.dao;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.rumata.adv.model.Campaign;
import org.rumata.adv.model.Placement;

import java.util.HashSet;
import java.util.Set;

@Ignore //нужно задавать настройки соединения с БД
public class CampaignDaoTest {

    private CampaignDao campaignDao = new CampaignDao();
    private PlacementDao placementDao = new PlacementDao();

    @Before
    public void clearDb() {
        placementDao.removeAll();
        campaignDao.removeAll();
    }

    @Test
    public void testSaveNew() {
        Campaign campaign = new Campaign();
        campaign.setName("campaignBefore");

        campaignDao.save(campaign);
        Assert.assertEquals(1, campaignDao.getAll().size());
    }

    @Test
    public void testRemove() {
        Campaign campaign = new Campaign();
        campaign.setName("campaignBefore");

        campaign = campaignDao.save(campaign);
        campaignDao.remove(campaign.getId());
        Assert.assertEquals(0, campaignDao.getAll().size());
    }

    @Test
    public void testUpdate() {
        Campaign campaign = new Campaign();
        campaign.setName("campaignBefore");
        campaign.setSlogan("campaignBefore forever!");
        campaign.setWeight((short) 1);

        campaign = campaignDao.save(campaign);
        Long campaignId = campaign.getId();
        campaign.setName("campaignAfter");
        campaign.setSlogan("campaignAfter forever");
        campaign.setWeight((short) 2);
        campaignDao.save(campaign);

        campaign = campaignDao.getById(campaignId);
        Assert.assertEquals("campaignAfter", campaign.getName());
        Assert.assertEquals("campaignAfter forever", campaign.getSlogan());
        Assert.assertEquals(Short.valueOf((short) 2), campaign.getWeight());
    }

    @Test
    public void testAddPlacement() {
        Campaign campaign = campaignDao.save(new Campaign());
        Placement placement = placementDao.save(new Placement());
        campaignDao.addPlacement(campaign.getId(), placement.getId());

        campaign = campaignDao.getById(campaign.getId());
        Assert.assertEquals(1, campaign.getPlacements().size());
    }

    @Test
    public void testRemovePlacement() {
        Campaign campaign = campaignDao.save(new Campaign());
        Placement placement = placementDao.save(new Placement());
        campaignDao.addPlacement(campaign.getId(), placement.getId());
        campaignDao.removePlacement(campaign.getId(), placement.getId());

        campaign = campaignDao.getById(campaign.getId());
        Assert.assertTrue(campaign.getPlacements().isEmpty());
    }

    @Test
    public void testGetCampaignsForPlacement() {
        Set<Campaign> campaigns = new HashSet<>();
        for (int i = 0; i < 5; i++) {
            campaigns.add(campaignDao.save(new Campaign()));
        }

        Placement placement = placementDao.save(new Placement());
        campaigns.forEach(campaign -> campaignDao.addPlacement(campaign.getId(), placement.getId()));

        Assert.assertEquals(campaigns, campaignDao.getCampaignsForPlacement(placement.getId()));
    }
}
