package org.rumata.adv.dao;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.rumata.adv.model.Placement;

@Ignore //нужно задавать настройки соединения с БД
public class PlacementDaoTest {

    private PlacementDao placementDao = new PlacementDao();

    @Before
    public void clearDb() {
        placementDao.removeAll();
    }

    @Test
    public void testSaveNew() {
        Placement placement = new Placement();
        placement.setName("placementBefore");

        placementDao.save(placement);
        Assert.assertEquals(1, placementDao.getAll().size());
    }

    @Test
    public void testRemove() {
        Placement placement = new Placement();
        placement.setName("placementBefore");

        placement = placementDao.save(placement);
        placementDao.remove(placement.getId());
        Assert.assertEquals(0, placementDao.getAll().size());
    }

    @Test
    public void testUpdate() {
        Placement placement = new Placement();
        placement.setName("placementBefore");

        placement = placementDao.save(placement);
        Long placementId = placement.getId();
        placement.setName("placementAfter");
        placementDao.save(placement);

        Assert.assertEquals("placementAfter", placementDao.getById(placementId).getName());
    }
}
