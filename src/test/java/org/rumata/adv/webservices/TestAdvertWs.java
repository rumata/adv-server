package org.rumata.adv.webservices;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.rumata.adv.dao.CampaignDao;
import org.rumata.adv.dao.PlacementDao;
import org.rumata.adv.model.Campaign;
import org.rumata.adv.model.Placement;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestAdvertWs {

    private CampaignDao campaignDao = new CampaignDao();
    private PlacementDao placementDao = new PlacementDao();

    @Before
    public void clearDb() {
        placementDao.removeAll();
        campaignDao.removeAll();
    }

    @Ignore //не проверяет корректность, просто выводит результат
    @Test
    public void testGetRandomSloganForPlacement() {
        Set<Campaign> campaigns = new HashSet<>();
        campaigns.add(campaignDao.save(createCampaign(1, "1")));
        campaigns.add(campaignDao.save(createCampaign(2, "2")));
        campaigns.add(campaignDao.save(createCampaign(3, "3")));

        Long placementId = placementDao.save(new Placement()).getId();
        campaigns.forEach(campaign -> campaignDao.addPlacement(campaign.getId(), placementId));

        AdvertWs advertWs = new AdvertWs();
        Map<String, Long> result = Stream
                .generate(() -> advertWs.getRandomSloganForPlacement(placementId))
                .limit(100)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        System.out.println(result);
    }


    /*---------------PRIVATE-------------*/

    private Campaign createCampaign(int weight, String slogan) {
        Campaign campaign = new Campaign();
        campaign.setWeight((short) weight);
        campaign.setSlogan(slogan);
        return campaign;
    }
}
