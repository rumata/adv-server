package org.rumata.adv.cache;


import org.rumata.adv.dao.CampaignDao;
import org.rumata.adv.dao.PlacementDao;
import org.rumata.adv.model.Campaign;
import org.rumata.adv.model.Placement;

import java.util.*;

/**
 * Класс кэширует результаты запросов за компаниями\
 * Внутри себя организует обновление данных в кэше по таймеру
 *
 * Singleton
 */
public class CampaignCache {

    private static final long SCHEDULER_DELAY = 1000;
    private static final long SCHEDULER_PERIOD = 50000;

    private static final CampaignCache instance = new CampaignCache();

    private volatile Map<Long, Set<Campaign>> campaignsForPlacement = Collections.emptyMap();
    private CampaignDao campaignDao = new CampaignDao();
    private PlacementDao placementDao = new PlacementDao();


    private CampaignCache() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                refresh();
            }
        }, SCHEDULER_DELAY, SCHEDULER_PERIOD);
    }


    public static CampaignCache getInstance() {
        return instance;
    }

    public Set<Campaign> getCampaignsForPlacement(long placementId) {
        return campaignsForPlacement.getOrDefault(placementId, Collections.emptySet());
    }


    private void refresh() {
//        System.out.println("refresh inited!");
        Map<Long, Set<Campaign>> campaignsForPlacement = new HashMap<>();
        for (Placement placement : placementDao.getAll()) {
            campaignsForPlacement.put(placement.getId(), campaignDao.getCampaignsForPlacement(placement.getId()));
        }
        this.campaignsForPlacement = Collections.unmodifiableMap(campaignsForPlacement);
    }
}
