package org.rumata.adv.dao;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Objects;
import java.util.Properties;

/**
 * Класс инкапсулирует работу с драйвером JDBC и коннектом к базе
 * Singleton
 */
class JdbcExecutor {

    private static final JdbcExecutor instance = new JdbcExecutor();

    private String jdbcUrl;
    private String jdbcDriver;
    private String jdbcUser;
    private String jdbcPassword;


    private JdbcExecutor() {
        Properties properties = getJdbcProperties();
        jdbcUrl = Objects.requireNonNull(properties.getProperty("jdbc.url"));
        jdbcDriver = Objects.requireNonNull(properties.getProperty("jdbc.driver"));
        jdbcUser = Objects.requireNonNull(properties.getProperty("jdbc.user"));
        jdbcPassword = Objects.requireNonNull(properties.getProperty("jdbc.password"));
    }

    static JdbcExecutor getInstance() {
        return instance;
    }


    <T> T execute(SqlFunction<Statement, T> operation) {
        try (Connection connection = createConnection();
             Statement statement = connection.createStatement()) {

            return operation.apply(statement);
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    /*----------------PRIVATE---------*/

    private Connection createConnection() {
        try {
            Class.forName(jdbcDriver);
            return DriverManager.getConnection(jdbcUrl, jdbcUser, jdbcPassword);
        } catch (ClassNotFoundException | SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    private Properties getJdbcProperties() {
        String propertyPath = System.getProperty("properties.path", "src/main/resources/properties/app.properties");
        try (InputStream is = new FileInputStream(propertyPath)) {
            Properties properties = new Properties();
            properties.load(is);
            return properties;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
