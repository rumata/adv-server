package org.rumata.adv.dao;

import java.sql.SQLException;

interface SqlFunction<T, R> {

    R apply(T t) throws SQLException;

}
