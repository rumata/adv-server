package org.rumata.adv.dao;

import org.rumata.adv.model.Placement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class PlacementDao {

    private final JdbcExecutor jdbcExecutor = JdbcExecutor.getInstance();


    public Placement save(Placement placement) {
        return jdbcExecutor.execute(statement -> {
            if (placement.getId() == null) {
                statement.executeUpdate(String.format("insert into PLACEMENT(name) values ('%s')", placement.getName()));

                ResultSet lastIdResult = statement.executeQuery("select last_value from placement_id_seq");
                lastIdResult.next();
                placement.setId(lastIdResult.getLong(1));
                return placement;
            } else {
                int rowsUpdated = statement.executeUpdate(
                        String.format("update PLACEMENT set name = '%s' where id = %d", placement.getName() , placement.getId()));
                if (rowsUpdated < 1) throw new IllegalArgumentException("No instance with id = " + placement.getId());
                return placement;
            }
        });
    }

    public Placement getById(long placementId) {
        return jdbcExecutor.execute(statement -> {
            ResultSet resultSet = statement.executeQuery(
                    "select * from PLACEMENT where id = " + placementId);
            if (resultSet.next()) {
                return convertNext(resultSet);
            }
            return null;
        });
    }


    public Set<Placement> getAll() {
        return jdbcExecutor.execute(statement -> {
            ResultSet resultSet = statement.executeQuery("select * from PLACEMENT");
            Set<Placement> allPlacements = new HashSet<>();
            while (resultSet.next()) {
                allPlacements.add(convertNext(resultSet));
            }
            return allPlacements;
        });
    }

    public void remove(long placementId) {
        jdbcExecutor.execute(statement -> statement.executeUpdate(
                "delete from PLACEMENT where id = " + placementId));
    }

    public void removeAll() {
        JdbcExecutor.getInstance().execute(statement -> {
            statement.executeUpdate("delete from PLACEMENT");
            return null;
        });
    }

    /*----------------PRIVATE---------*/

    static Placement convertNext(ResultSet resultSet) throws SQLException {
        Placement placement = new Placement();
        placement.setId(resultSet.getLong("id"));
        placement.setName(resultSet.getString("name").trim());
        return placement;
    }
}
