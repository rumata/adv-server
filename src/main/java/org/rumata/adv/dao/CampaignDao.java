package org.rumata.adv.dao;

import org.rumata.adv.model.Campaign;
import org.rumata.adv.model.Placement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class CampaignDao {

    private final JdbcExecutor jdbcExecutor = JdbcExecutor.getInstance();


    public Campaign save(Campaign campaign) {
        return jdbcExecutor.execute(statement -> {
            if (campaign.getId() == null) {
                statement.executeUpdate(String.format("insert into CAMPAIGN(name, slogan, weight) values ('%s', '%s', %d)",
                                campaign.getName(), campaign.getSlogan(), campaign.getWeight()));

                ResultSet lastIdResult = statement.executeQuery("select last_value from campaign_id_seq");
                lastIdResult.next();
                campaign.setId(lastIdResult.getLong(1));
                return campaign;
            } else {
                int rowsUpdated = statement.executeUpdate(
                        String.format("update CAMPAIGN set name = '%s', slogan = '%s', weight = %d where id = %d",
                                campaign.getName(), campaign.getSlogan(), campaign.getWeight(), campaign.getId()));
                if (rowsUpdated < 1) throw new IllegalArgumentException("No instance with id = " + campaign.getId());
                return campaign;
            }
        });
    }

    public Campaign getById(long campaignId) {
        Campaign campaign = jdbcExecutor.execute(statement -> {
            ResultSet resultSet = statement.executeQuery(
                    "select * from CAMPAIGN where id = " + campaignId);
            if (resultSet.next()) {
                return convertNext(resultSet);
            }
            return null;
        });

        if (campaign != null) {
            addPlacementsToCampaign(campaign);
        }
        return campaign;
    }


    public Set<Campaign> getAll() {
        return jdbcExecutor.execute(statement -> {
            ResultSet resultSet = statement.executeQuery("select * from CAMPAIGN");
            Set<Campaign> allCampaigns = new HashSet<>();
            while (resultSet.next()) {
                allCampaigns.add(convertNext(resultSet));
            }
            return allCampaigns;
        });
    }

    public void remove(long campaignId) {
        jdbcExecutor.execute(statement -> statement.executeUpdate(
                "delete from CAMPAIGN where id = " + campaignId));
    }

    public void addPlacement(long campaignId, long placementId) {
        jdbcExecutor.execute(statement -> statement.executeUpdate(String.format(
                "insert into PlacementToCampaign(campaign_id, placement_id) values (%d, %d)",
                campaignId, placementId)));
    }

    public void removePlacement(long campaignId, long placementId) {
        jdbcExecutor.execute(statement -> statement.executeUpdate(String.format(
                "delete from PlacementToCampaign where campaign_id = %d and placement_id = %d",
                campaignId, placementId)));
    }

    public Set<Campaign> getCampaignsForPlacement(long placementId) {
//        System.out.println("getCampaignsForPlacement inited!");
        return jdbcExecutor.execute(statement -> {
            ResultSet resultSet = statement.executeQuery(String.format(
                    "select c.* from CAMPAIGN c " +
                            "join PlacementToCampaign ptc on c.id = ptc.campaign_id " +
                            "where ptc.placement_id = %d", placementId));
            Set<Campaign> campaigns = new HashSet<>();
            while (resultSet.next()) {
                campaigns.add(convertNext(resultSet));
            }
            return campaigns;
        });
    }

    public void removeAll() {
        JdbcExecutor.getInstance().execute(statement -> {
            statement.executeUpdate("delete from CAMPAIGN");
            return null;
        });
    }


    /*----------------PRIVATE---------*/

    static Campaign convertNext(ResultSet resultSet) throws SQLException {
        Campaign campaign = new Campaign();
        campaign.setId(resultSet.getLong("id"));
        campaign.setName(resultSet.getString("name").trim());
        campaign.setSlogan(resultSet.getString("slogan").trim());
        campaign.setWeight(resultSet.getShort("weight"));
        return campaign;
    }

    private void addPlacementsToCampaign(Campaign campaign) {
        if (campaign.getId() == null) return;
        campaign.setPlacements(jdbcExecutor.execute(statement -> {
            ResultSet resultSet = statement.executeQuery(String.format(
                    "select p.* from Placement p " +
                        "join PlacementToCampaign ptc on p.id = ptc.placement_id " +
                        "where ptc.campaign_id = %d", campaign.getId()));
            Set<Placement> placements = new HashSet<>();
            while (resultSet.next()) {
                placements.add(PlacementDao.convertNext(resultSet));
            }
            return placements;
        }));
    }

}
