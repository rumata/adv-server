package org.rumata.adv;


import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.rumata.adv.cache.CampaignCache;

/**
 * Starts jetty-server on the specified port
 */
public class Launcher {

    public static void main(String[] args) throws Exception {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
        context.setContextPath("/");

        ServletHolder jerseyServlet = context.addServlet(ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(0);
        jerseyServlet.setInitParameter("jersey.config.server.provider.packages", "org.rumata.adv");

        initScheduler();

        Server jettyServer = new Server(getPort(args));
        jettyServer.setHandler(context);
        jettyServer.start();
        jettyServer.join();
    }

    private static int getPort(String[] args ) {
        try {
            if (args.length > 0) {
                return Integer.parseInt(args[0]);
            }
        } catch (Exception e) {
            /**/
        }
        return 12135;
    }

    private static void initScheduler() {
        CampaignCache.getInstance();
    }
}

