package org.rumata.adv.webservices;

import org.rumata.adv.dao.PlacementDao;
import org.rumata.adv.model.Placement;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/placement")
public class PlacementWs {

    private PlacementDao placementDao = new PlacementDao();

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Placement save(Placement placement) {
        return placementDao.save(placement);
    }

    @GET
    @Path("/getById")
    @Produces(MediaType.APPLICATION_JSON)
    public Placement getById(@QueryParam("placementId") long placementId) {
        return placementDao.getById(placementId);
    }

    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Placement> getAll() {
        return placementDao.getAll();
    }

    @POST
    @Path("/remove")
    public void remove(@QueryParam("placementId") long placementId) {
        placementDao.remove(placementId);
    }
}
