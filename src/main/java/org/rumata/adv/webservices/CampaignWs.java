package org.rumata.adv.webservices;

import org.rumata.adv.dao.CampaignDao;
import org.rumata.adv.model.Campaign;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Set;

@Path("/campaign")
public class CampaignWs {

    private final CampaignDao campaignDao = new CampaignDao();

    @POST
    @Path("/save")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Campaign save(Campaign campaign) {
        return campaignDao.save(campaign);
    }

    @GET
    @Path("/getById")
    @Produces(MediaType.APPLICATION_JSON)
    public Campaign getById(@QueryParam("campaignId") long campaignId) {
        return campaignDao.getById(campaignId);
    }

    @GET
    @Path("/getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Campaign> getAll() {
        return campaignDao.getAll();
    }

    @POST
    @Path("/remove")
    public void remove(@QueryParam("campaignId") long campaignId) {
        campaignDao.remove(campaignId);
    }

    @POST
    @Path("/addPlacement")
    public void addPlacement(@QueryParam("campaignId") long campaignId, @QueryParam("placementId") long placementId) {
        campaignDao.addPlacement(campaignId, placementId);
    }

    @POST
    @Path("/removePlacement")
    public void removePlacement(@QueryParam("campaignId") long campaignId, @QueryParam("placementId") long placementId) {
        campaignDao.removePlacement(campaignId, placementId);
    }
}
