package org.rumata.adv.webservices;

import org.rumata.adv.cache.CampaignCache;
import org.rumata.adv.model.Campaign;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import java.util.Set;
import java.util.function.Function;

@Path("/")
public class AdvertWs {

//    private Function<Long, Set<Campaign>> campaignSearcher = new CampaignDao()::getCampaignsForPlacement;
    private Function<Long, Set<Campaign>> campaignSearcher = CampaignCache.getInstance()::getCampaignsForPlacement;

    @GET
    @Path("/imp")
    public String getRandomSloganForPlacement(@QueryParam("pl") long placementId) {
        Set<Campaign> campaigns = campaignSearcher.apply(placementId);
        int allWeightSum = campaigns.stream().mapToInt(Campaign::getWeight).sum();
        double randomInAllSumRange = Math.random() * allWeightSum;

        int partialWeightSum = 0;
        for (Campaign campaign : campaigns) {
            partialWeightSum += campaign.getWeight();
            if (partialWeightSum >= randomInAllSumRange) {
                return campaign.getSlogan();
            }
        }
        return null;
    }
}
