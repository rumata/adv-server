#Создать площадки
curl -X POST http://localhost:12135/placement/save -d "{\"name\":\"place 1\"}" -H "Content-Type: application/json"
curl -X POST http://localhost:12135/placement/save -d "{\"name\":\"place 2\"}" -H "Content-Type: application/json"
curl -X POST http://localhost:12135/placement/save -d "{\"name\":\"place 3\"}" -H "Content-Type: application/json"
curl -X POST http://localhost:12135/placement/save -d "{\"name\":\"place 4\"}" -H "Content-Type: application/json"
curl -X POST http://localhost:12135/placement/save -d "{\"name\":\"place 5\"}" -H "Content-Type: application/json"

#Изменить площадку
curl -X POST http://localhost:12135/placement/save -d "{\"id\":20,\"name\":\"place 5!!!\"}" -H "Content-Type: application/json"

#Посмотреть все площадки
curl http://localhost:12135/placement/getAll

#Посмотреть конкретную площадку
curl http://localhost:12135/placement/getById?placementId=20

#Удалить площадку (последним параметром подставляем id)
curl -X POST http://localhost:12135/placement/remove?placementId=20


#Создать фирмы
curl -X POST http://localhost:12135/campaign/save -d "{\"name\":\"one\", \"slogan\":\"one forever!\", \"weight\":1}" -H "Content-Type: application/json"
curl -X POST http://localhost:12135/campaign/save -d "{\"name\":\"two\", \"slogan\":\"two forever!\", \"weight\":2}" -H "Content-Type: application/json"
curl -X POST http://localhost:12135/campaign/save -d "{\"name\":\"three\", \"slogan\":\"three forever!\", \"weight\":3}" -H "Content-Type: application/json"

#Изменить фирму
curl -X POST http://localhost:12135/campaign/save -d "{\"id\":4, {\"name\":\"three\", \"slogan\":\"three forever!\", \"weight\":3}" -H "Content-Type: application/json"

#Назначить площадки к фирмам (первым параметром - id фирмы, вторым - id площадки)
curl -X POST "http://localhost:12135/campaign/addPlacement?campaignId=2&placementId=16"
curl -X POST "http://localhost:12135/campaign/addPlacement?campaignId=2&placementId=17"
curl -X POST "http://localhost:12135/campaign/addPlacement?campaignId=2&placementId=18"
curl -X POST "http://localhost:12135/campaign/addPlacement?campaignId=3&placementId=16"
curl -X POST "http://localhost:12135/campaign/addPlacement?campaignId=3&placementId=17"
curl -X POST "http://localhost:12135/campaign/addPlacement?campaignId=3&placementId=18"

#Открепить площадку от фирмы (первым параметром - id фирмы, вторым - id площадки)
curl -X POST "http://localhost:12135/campaign/removePlacement?campaignId=3&placementId=18"

#Посмотреть все фирмы (в таком режиме площадки у фирм не отображаются)
curl http://localhost:12135/campaign/getAll

#Посмотреть конкретную фирму и площадки под нее (последним параметром подставляем id)
curl http://localhost:12135/campaign/getById?campaignId=2

#Удалить фирму (последним параметром подставляем id)
curl -X POST http://localhost:12135/campaign/remove?campaignId=4