CREATE TABLE CAMPAIGN
(
    id serial NOT NULL,
    name character(128),
    slogan character(1024),
    weight smallint NOT NULL,
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE CAMPAIGN
    OWNER to postgres;