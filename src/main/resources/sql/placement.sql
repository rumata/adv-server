CREATE TABLE PLACEMENT
(
    id SERIAL,
    name character(128),
    PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE PLACEMENT
    OWNER to postgres;