CREATE TABLE PlacementToCampaign
(
    campaign_id integer,
    placement_id integer,
    CONSTRAINT campaign_id FOREIGN KEY (campaign_id)
        REFERENCES public.campaign (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE,
    CONSTRAINT placement_id FOREIGN KEY (placement_id)
        REFERENCES public.placement (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE PlacementToCampaign
    OWNER to postgres;